import { IQeueDetailsRepository } from '../db/types';
import { QueueDetailsUpdate, QueueDetails, QueueDetailsCreate } from '../types/queue';
import * as uuid from 'uuid';

export interface IQueueDetailsService {
  deleteQueueDetailsById: (id: string) => Promise<void>;
  updateQueueDetails: (data: QueueDetailsUpdate) => Promise<QueueDetails>;
  findQueueDetailsById: (id: string) => Promise<QueueDetails>;
  createQueueDetails: (data: QueueDetailsCreate) => Promise<QueueDetails>;
  getGroupIdByQueueDetailsId: (id: string) => Promise<string>;
}

export class QueueDetailsService implements IQueueDetailsService {
  private queueDetailsRepository: IQeueDetailsRepository;

  constructor(queueDetailsRepository: IQeueDetailsRepository) {
    this.queueDetailsRepository = queueDetailsRepository;
  }

  public async getGroupIdByQueueDetailsId(id: string): Promise<string> {
    const groupId = await this.queueDetailsRepository.getGroupIdByQueueDetailsId(id);
    if (!groupId) {
      throw { status: 404, errorMessage: `QueueDetails with id=${id} not found!` };
    }
    return groupId;
  }

  public async deleteQueueDetailsById(id: string): Promise<void> {
    return this.queueDetailsRepository.deleteQueueDetailsById(id);
  }

  public async updateQueueDetails(data: QueueDetailsUpdate): Promise<QueueDetails> {
    const queueDetails = await this.queueDetailsRepository.updateQueueDetails(data);
    if (!queueDetails) {
      throw { status: 404, errorMessage: 'QueueDetails not found!' };
    }
    return queueDetails;
  }

  public async findQueueDetailsById(id: string): Promise<QueueDetails> {
    const queueDetails = await this.queueDetailsRepository.findQueueDetailsById(id);
    if (!queueDetails) {
      throw { status: 404, errorMessage: `QueueDetails with id=${id} not found!` };
    }
    return queueDetails;
  }

  public async createQueueDetails(data: QueueDetailsCreate): Promise<QueueDetails> {
    const queueDetails = {
      ...data,
      id: uuid.v4(),
    } as QueueDetails;
    await this.queueDetailsRepository.saveQueueDetails(queueDetails);
    return queueDetails;
  }
}
