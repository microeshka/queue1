import { IQeueRepository } from '../db/types';
import { Queue, QueueCreate, QueueMembers, QueueUpdate } from '../types/queue';
import * as uuid from 'uuid';

export interface IQueueService {
  createQueue: (data: QueueCreate) => Promise<Queue>;
  findQueueById: (id: string) => Promise<Queue>;
  updateQueue: (group: QueueUpdate) => Promise<Queue>;
  deleteQueueById: (id: string) => Promise<void>;
  findQueuesBySubjectId: (subjectId: string) => Promise<Queue[]>;
  loadMembers: (id: string) => Promise<QueueMembers[]>;
  getGroupIdBySubjectId: (id: string) => Promise<string>;
  getGroupIdByQueueId: (id: string) => Promise<string>;
}

export class QueueService implements IQueueService {
  private queueRepository: IQeueRepository;

  constructor(queueRepository: IQeueRepository) {
    this.queueRepository = queueRepository;
  }

  public async getGroupIdBySubjectId(id: string): Promise<string> {
    const groupId = await this.queueRepository.getGroupIdBySubjectId(id);
    if (!groupId) {
      throw { status: 404, errorMessage: `Subject with id=${id} not found!` };
    }
    return groupId;
  }

  public async getGroupIdByQueueId(id: string): Promise<string> {
    const groupId = await this.queueRepository.getGroupIdByQueueId(id);
    if (!groupId) {
      throw { status: 404, errorMessage: `Queue with id=${id} not found!` };
    }
    return groupId;
  }

  public async createQueue(data: QueueCreate): Promise<Queue> {
    const queue = {
      ...data,
      id: uuid.v4(),
      creationDate: new Date(),
    } as Queue;
    await this.queueRepository.saveQueue(queue);
    return queue;
  }

  public async findQueueById(id: string): Promise<Queue> {
    const queue = await this.queueRepository.findQueueById(id);
    if (!queue) {
      throw { status: 404, errorMessage: `Queue with id=${id} not found!` };
    }
    return queue;
  }

  public async updateQueue(data: QueueUpdate): Promise<Queue> {
    const queue = await this.queueRepository.updateQueue(data);
    if (!queue) {
      throw { status: 404, errorMessage: `Queue with id=${data.id} not found!` };
    }
    return queue;
  }

  public async deleteQueueById(id: string): Promise<void> {
    return this.queueRepository.deleteQueueById(id);
  }

  public async findQueuesBySubjectId(subjectId: string): Promise<Queue[]> {
    return this.queueRepository.findQueuesBySubjectId(subjectId);
  }

  public async loadMembers(id: string): Promise<QueueMembers[]> {
    return this.queueRepository.loadMembers(id);
  }
}
