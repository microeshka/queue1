import { PoolConfig } from 'pg';
import { createFastifyServer } from './config/fastifyServer';
import { QueueDetailsRepository } from './db/queueDetaildRepository';
import { QueueRepository } from './db/queueRepository';
import { PermissionChecker } from './interceptor';
import { IQueueDetailsService, QueueDetailsService } from './services/queueDetailsService';
import { IQueueService, QueueService } from './services/queueService';
import { checkAndGetEnv } from './utils/utils';

export class Application {
  private queueService: IQueueService;
  private queueDetailsService: IQueueDetailsService;
  private fastifyServer: any;

  constructor() {
    const dbConfig = {
      max: 20,
      idleTimeoutMillis: 10000,
      connectionTimeoutMillis: 2000,
      connectionString: checkAndGetEnv('MAIN_DB_CONNECTION_STRING'),
    } as PoolConfig;
    const queueRepository = new QueueRepository(dbConfig);
    const queueDetailsRepository = new QueueDetailsRepository(dbConfig);
    this.queueService = new QueueService(queueRepository);
    this.queueDetailsService = new QueueDetailsService(queueDetailsRepository);
    this.fastifyServer = createFastifyServer(this.queueService, this.queueDetailsService, new PermissionChecker());
  }

  public startFastifyServer() {
    const port = checkAndGetEnv('HTTP_SERVER_PORT');
    this.fastifyServer.listen({ port }).then(() =>
      console.log(`App started! PORT: ${port}`),
    );
  }
}
