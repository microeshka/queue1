import fastify, { FastifyReply, FastifyRequest, HookHandlerDoneFunction } from 'fastify';
import cors from 'fastify-cors';
import fastifyCookie from 'fastify-cookie';
import { RouteGenericInterface } from 'fastify/types/route';
import { IncomingMessage, ServerResponse } from 'http';
import { healthCheckPlugin } from '../plugins/healthCheck';
import { Server } from 'http';
import { IQueueService } from '../services/queueService';
import { IQueueDetailsService } from '../services/queueDetailsService';
import { queuePlugin } from '../plugins/queue';
import { queueDetailsPlugin } from '../plugins/queueDetails';
import { Interceptor } from '../interceptor';

const allowUnauthorized = new Set(['/', '/healthz', '/queue/health']);

const cookieHook = (
    request: FastifyRequest<RouteGenericInterface, Server, IncomingMessage>,
    reply: FastifyReply<Server, IncomingMessage, ServerResponse, RouteGenericInterface, unknown>,
    done: HookHandlerDoneFunction
) => {
  if (!allowUnauthorized.has(request.url) && !request.cookies['Session']) {
    reply.code(401).send('User is not authorized');
  } else {
    done();
  }
};

export const createFastifyServer = (queueService: IQueueService, queueDetailsService: IQueueDetailsService, interceptor: Interceptor) => {
  const server = fastify({});
  server.register(cors, {
    origin: (_origin, cb) => {
      cb(null, true);
    },
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    credentials: true,
  });
  server.register(fastifyCookie);
  server.register(healthCheckPlugin, {});
  server.register(queuePlugin, { queueService, interceptor });
  server.register(queueDetailsPlugin, { queueDetailsService, interceptor, queueService });
  server.addHook('onRequest', cookieHook);
  return server;
};
