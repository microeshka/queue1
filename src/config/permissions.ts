
export const permissionsConfig = {
  'createQueue': 'perm.queue.own.edit',
  'updateQueue': 'perm.queue.own.edit',
  'deleteQueue': 'perm.queue.own.edit',
  'readQueue': 'perm.queue.own.edit.read',
  'turnIn': 'perm.queue.own.edit.turnin',
} as { [key: string]: string };
