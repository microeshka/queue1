
export interface Queue extends QueueCreate {
  id: string;
  subjectId: string;
  closingDate?: Date;
  endDate: Date;
  creationDate: Date;
}

export interface QueueCreate {
  title: string;
  description: string;
  subjectId: string;
  startDate: Date;
  endDate: Date;
  creatorId?: string;
}

export interface QueueUpdate extends QueueCreate {
  id: string;
  closingDate?: Date;
}

export interface QueueDetailsUpdate {
  queueId: string;
  userId: string;
  passed?: boolean
}

export interface QueueDetailsCreate {
  queueId: string;
  sequenceNumber: number;
}

export interface QueueDetails extends QueueDetailsCreate, QueueDetailsUpdate {
  id: string;
}

export interface QueueDetailsTurnIn extends QueueDetailsCreate, QueueDetailsUpdate {
}

export interface QueueMembers {
  user: {
    id: string;
    fullName: string
  };
  sequenceNumber: number;
  turnedAt: Date;
  passed: boolean;
}

export interface UserSession {
  id: string;
  userId: string;
  username: string;
  fullName: string;
  validUntil: Date;
}

export interface Permission {
  id: string;
  name: string;
}
