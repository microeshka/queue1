import { FastifyPluginCallback, RouteShorthandOptions } from 'fastify';
import { findSession, getPermissions } from '../grpc/grpcClient';
import { Interceptor } from '../interceptor';
import { IQueueDetailsService } from '../services/queueDetailsService';
import { IQueueService } from '../services/queueService';
import { QueueDetailsCreate, QueueDetailsUpdate } from '../types/queue';

export interface IQueueDetailsPluginOptions {
  queueDetailsService: IQueueDetailsService;
  queueService: IQueueService;
  interceptor: Interceptor;
}

const createQueueDetailsSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['userId', 'queueId', 'passed', 'sequenceNumber'],
      properties: {
        userId: { type: 'string' },
        queueId: { type: 'string' },
        passed: { type: 'boolean' },
        sequenceNumber: { type: 'number' },
      },
    },
  },
};

export const queueDetailsPlugin: FastifyPluginCallback<IQueueDetailsPluginOptions> = (fastify, options, done) => {
  fastify.post('/queue/details', createQueueDetailsSchema, async (request, response) => {
    const data = request.body as QueueDetailsCreate;
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdByQueueId(data.queueId);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'turnIn', permissions, () => options.queueDetailsService.createQueueDetails(data)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.put('/queue/details', async (request, response) => {
    const queueDetailData = request.body as QueueDetailsUpdate;
    try {
      const session = await findSession(request.cookies['Session']);
      if (queueDetailData.userId !== session.userId) {
        throw { status: 403, errorMessage: 'Permission denied. QueueDetails doesn\'t belong to authenticated user\'s!' };
      }
      response.code(200).send(JSON.stringify(await options.queueDetailsService.updateQueueDetails(queueDetailData)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/queue/details/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueDetailsService.getGroupIdByQueueDetailsId(id);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readQueue', permissions, () => options.queueDetailsService.findQueueDetailsById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.delete('/queue/details/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const queueDetails = await options.queueDetailsService.findQueueDetailsById(id);
      if (queueDetails.userId !== session.userId) {
        throw { status: 403, errorMessage: `Permission denied. QueueDetails with id=${id} doesn't belong to authenticated user's!` };
      }
      await options.queueDetailsService.deleteQueueDetailsById(id);
      response.code(204);
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  done();
};
