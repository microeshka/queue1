import { FastifyPluginCallback, RouteShorthandOptions } from 'fastify';
import { findSession, getPermissions } from '../grpc/grpcClient';
import { Interceptor } from '../interceptor';
import { IQueueService } from '../services/queueService';
import { QueueCreate, QueueUpdate } from '../types/queue';

export interface IQueuePluginOptions {
  queueService: IQueueService;
  interceptor: Interceptor;
}

const createQueueSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['title', 'description', 'subjectId'],
      properties: {
        title: { type: 'string' },
        description: { type: 'string' },
        subjectId: { type: 'string' },
        startDate: { type: 'string' },
        endDate: { type: 'string' },
      },
    },
  },
};

const updateQueueSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['id', 'title', 'description', 'subjectId'],
      properties: {
        id: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        subjectId: { type: 'string' },
        startDate: { type: 'string' },
        endDate: { type: 'string' },
      },
    },
  },
};

export const queuePlugin: FastifyPluginCallback<IQueuePluginOptions> = (fastify, options, done) => {
  fastify.post('/queue', createQueueSchema, async (request, response) => {
    const data = request.body as QueueCreate;
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdBySubjectId(data.subjectId);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      data.creatorId = session.userId;
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'createQueue', permissions, () => options.queueService.createQueue(data)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.put('/queue', updateQueueSchema, async (request, response) => {
    const data = request.body as QueueUpdate;
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdBySubjectId(data.subjectId);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'updateQueue', permissions, () => options.queueService.updateQueue(data)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/queue/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdByQueueId(id);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readQueue', permissions, () => options.queueService.findQueueById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/queue/all/:subjectId', async (request, response) => {
    const { subjectId } = request.params as { subjectId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdBySubjectId(subjectId);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readQueue', permissions, () => options.queueService.findQueuesBySubjectId(subjectId)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.get('/queue/:queueId/member', async (request, response) => {
    const { queueId } = request.params as { queueId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdByQueueId(queueId);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readQueue', permissions, () => options.queueService.loadMembers(queueId)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  fastify.delete('/queue/delete/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const groupId = await options.queueService.getGroupIdByQueueId(id);
      const permissions = (await getPermissions(groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'deleteQueue', permissions, () => options.queueService.deleteQueueById(id)
      );
      response.code(204);
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || 'Internal server error');
    }
  });

  done();
};
