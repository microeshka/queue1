// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var group_pb = require('./group_pb.js');

function serialize_group_PermissionsRequest(arg) {
  if (!(arg instanceof group_pb.PermissionsRequest)) {
    throw new Error('Expected argument of type group.PermissionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_group_PermissionsRequest(buffer_arg) {
  return group_pb.PermissionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_group_PermissionsResponse(arg) {
  if (!(arg instanceof group_pb.PermissionsResponse)) {
    throw new Error('Expected argument of type group.PermissionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_group_PermissionsResponse(buffer_arg) {
  return group_pb.PermissionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var GroupService = exports.GroupService = {
  getUserPermissions: {
    path: '/group.Group/getUserPermissions',
    requestStream: false,
    responseStream: false,
    requestType: group_pb.PermissionsRequest,
    responseType: group_pb.PermissionsResponse,
    requestSerialize: serialize_group_PermissionsRequest,
    requestDeserialize: deserialize_group_PermissionsRequest,
    responseSerialize: serialize_group_PermissionsResponse,
    responseDeserialize: deserialize_group_PermissionsResponse,
  },
};

exports.GroupClient = grpc.makeGenericClientConstructor(GroupService);
