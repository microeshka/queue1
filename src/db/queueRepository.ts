import { Pool, PoolConfig } from 'pg';
import { Queue, QueueUpdate, QueueMembers } from '../types/queue';
import { IQeueRepository } from './types';

export class QueueRepository implements IQeueRepository {
  private connectionPool: Pool;

  constructor(opts: PoolConfig) {
    try {
      this.connectionPool = new Pool(opts);
    } catch (e) {
      console.error(`Failed to connect to Postgres: ${e}`);
      throw e;
    }
  }

  public async updateQueue(queueUpdate: QueueUpdate): Promise<Queue | undefined> {
    const queue = await this.findQueueById(queueUpdate.id);
    if (!queue) {
      throw { status: 404, errorMessage: `queues with id=${queueUpdate.id} does not exist` };
    }
    if (queueUpdate.title) {
      queue.title = queueUpdate.title;
    }
    if (queueUpdate.description) {
      queue.description = queueUpdate.description;
    }
    if (queueUpdate.startDate) {
      queue.startDate = queueUpdate.startDate;
    }
    if (queueUpdate.endDate) {
      queue.endDate = queueUpdate.endDate;
    }
    if (queueUpdate.closingDate) {
      queue.closingDate = queueUpdate.closingDate;
    }
    const client = await this.connectionPool.connect();
    await client.query(
        'update queues set title=$2, description=$3, start_date=$4, end_date=$5, closing_date=$6 where id=$1',
        [
          queue.id,
          queue.title,
          queue.description,
          queue.startDate,
          queue.endDate,
          queue.closingDate,
        ]);
    client.release();
    return queue;
  }

  public async deleteQueueById(id: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('delete from queues where id = $1', [id],);
    client.release();
  }

  public async findQueueById(id: string): Promise<Queue | undefined> {
    const client = await this.connectionPool.connect();
    const rows = (await client.query(
        'select * from queues where id = $1',
        [id],
    )).rows;
    client.release();
    const rawQueue = rows[0];
    return {
      id: rawQueue.id,
      title: rawQueue.title,
      description: rawQueue.description,
      subjectId: rawQueue['subject_id'],
      creatorId: rawQueue['creator_id'],
      creationDate: new Date(rawQueue['created_at']),
      startDate: new Date(rawQueue['start_date']),
      endDate: new Date(rawQueue['end_date']),
      closingDate: rawQueue['closing_date'] == null ? undefined : new Date(rawQueue['closing_date']),
    } as Queue;
  }

  public async saveQueue(queue: Queue): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query(
        'insert into queues (id, title, description, subject_id, creator_id, created_at, start_date, end_date) values ($1, $2, $3, $4, $5, $6, $7, $8)',
        [
          queue.id,
          queue.title,
          queue.description,
          queue.subjectId,
          queue.creatorId,
          queue.creationDate,
          queue.startDate,
          queue.endDate,
        ]);
    client.release();
  }

  public async findQueuesBySubjectId(subjectId: string): Promise<Queue[]> {
    const client = await this.connectionPool.connect();
    const rawQueues = (await client.query(
        'select * from queues where subject_id=$1',
        [subjectId])).rows as any;
    client.release();
    const queues = rawQueues.map((queue: any) => {
      return {
        id: queue.id,
        title: queue.title,
        description: queue.description,
        creationDate: queue['created_at'],
        startDate: new Date(queue['start_date']),
        endDate: new Date(queue['end_date']),
        closingDate: queue['closing_date'] == null ? undefined : new Date(queue['closing_date']),
        subjectId: queue['subject_id'],
        creatorId: queue['creator_id'],
      } as Queue;
    });
    return queues;
  }

  public async loadMembers(id: string): Promise<QueueMembers[]> {
    const client = await this.connectionPool.connect();
    const rows = (await client.query(
        'select full_name,user_id,sequence_number, turned_at, passed ' +
      'from queue_details  ' +
      'join users on queue_details.user_id = users.id ' +
      'where queue_details.queue_id = $1',
        [id]
    )).rows;
    client.release();
    const members = rows.map((data: any) => {
      return {
        user: {
          id: data['user_id'],
          fullName: data['full_name'],
        },
        sequenceNumber: data['sequence_number'],
        turnedAt: data['turned_at'],
        passed: data['passed'],
      } as QueueMembers;
    });
    return members;
  }

  public async getGroupIdBySubjectId(id: string): Promise<string | undefined> {
    const client = await this.connectionPool.connect();
    const groupId = (await client.query('select group_id as id from subjects where id=$1', [id])).rows.map((r) => r.id)[0];
    client.release();
    return groupId;
  }

  public async getGroupIdByQueueId(id: string): Promise<string | undefined> {
    const client = await this.connectionPool.connect();
    const groupId = (await client.query(
        'select s.group_id as id from queues q left join subjects s on s.id = q.subject_id where q.id=$1',
        [id])).rows.map((r) => r.id)[0];
    client.release();
    return groupId;
  }
}
