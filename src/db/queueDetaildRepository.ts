import { Pool, PoolConfig } from 'pg';
import { QueueDetails, QueueDetailsUpdate } from '../types/queue';
import { IQeueDetailsRepository } from './types';

export class QueueDetailsRepository implements IQeueDetailsRepository {
  private connectionPool: Pool;

  constructor(opts: PoolConfig) {
    try {
      this.connectionPool = new Pool(opts);
    } catch (e) {
      console.error(`Failed to connect to Postgres: ${e}`);
      throw e;
    }
  }

  public async deleteQueueDetailsById(id: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('delete from queue_details where id = $1', [id],);
    client.release();
  }

  public async findQueueDetailsById(id: string): Promise<QueueDetails | undefined> {
    const client = await this.connectionPool.connect();
    const rows = (await client.query(
        'select * from queue_details where id = $1',
        [id],
    )).rows;
    client.release();
    const rawQueueDetails = rows[0];
    return {
      id: rawQueueDetails.id,
      queueId: rawQueueDetails['queue_id'],
      userId: rawQueueDetails['user_id'],
      sequenceNumber: rawQueueDetails['sequence_number'],
      passed: rawQueueDetails.passed,
    };
  }

  public async saveQueueDetails(queueDetails: QueueDetails): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query(
        'insert into queue_details (id, queue_id, user_id, sequence_number, passed) values ($1, $2, $3, $4, $5)',
        [
          queueDetails.id,
          queueDetails.queueId,
          queueDetails.userId,
          queueDetails.sequenceNumber,
          queueDetails.passed,
        ]);
    client.release();
  }

  public async updateQueueDetails(queueDetailsUpdate: QueueDetailsUpdate): Promise<QueueDetails | undefined> {
    const client = await this.connectionPool.connect();
    await client.query(
        'update queue_details set passed=$3 where user_id=$1 and queue_id=$2',
        [
          queueDetailsUpdate.userId,
          queueDetailsUpdate.queueId,
          queueDetailsUpdate.passed,
        ]);
    client.release();
    // remove this danger code!
    return {} as any;
  }

  public async getGroupIdByQueueDetailsId(id: string): Promise<string | undefined> {
    const client = await this.connectionPool.connect();
    const groupId = (await client.query(
        'select s.group_id as id from queue_details qd left join queues q on q.id = qd.queue_id left join subjects s on s.id = q.subject_id where qd.id=$1',
        [id])).rows.map((r) => r.id)[0];
    client.release();
    return groupId;
  }
}
