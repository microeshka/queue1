import { Queue, QueueDetails, QueueDetailsUpdate, QueueUpdate, QueueMembers } from '../types/queue';

export interface IQeueRepository {
  saveQueue: (user: Queue) => Promise<void>;
  findQueueById: (id: string) => Promise<Queue | undefined>;
  updateQueue: (group: QueueUpdate) => Promise<Queue | undefined>;
  deleteQueueById: (id: string) => Promise<void>;
  findQueuesBySubjectId: (subjectId: string) => Promise<Queue[]>;
  loadMembers: (id: string) => Promise<QueueMembers[]>;
  getGroupIdBySubjectId: (id: string) => Promise<string | undefined>;
  getGroupIdByQueueId: (id: string) => Promise<string | undefined>;
}

export interface IQeueDetailsRepository {
  deleteQueueDetailsById: (id: string) => Promise<void>;
  updateQueueDetails: (group: QueueDetailsUpdate) => Promise<QueueDetails | undefined>;
  findQueueDetailsById: (id: string) => Promise<QueueDetails | undefined>;
  saveQueueDetails: (user: QueueDetails) => Promise<void>;
  getGroupIdByQueueDetailsId: (id: string) => Promise<string | undefined>;
}
